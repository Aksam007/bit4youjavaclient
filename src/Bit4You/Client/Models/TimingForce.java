package Bit4You.Client.Models;

public enum TimingForce {
    DAY,
    GTC,
    OPG,
    GTD,
    IOC,
    FOK
}
