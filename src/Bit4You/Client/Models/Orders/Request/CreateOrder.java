package Bit4You.Client.Models.Orders.Request;

import Bit4You.Client.Models.TimingForce;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;

public class CreateOrder {
    @SerializedName("market")
    private String Market;
        //[System.Text.Json.Serialization.JsonConverter(typeof(StringEnumConverter))]
    @SerializedName("type")
    private String Type;
    @SerializedName("quantity")
    private int Quantity;
    @SerializedName("quantity_iso")
    private String QuantityIso;
    private double Rate;
    @SerializedName("clientid")
    private String ClientId;
    @SerializedName("timingforce")
    private TimingForce TimingForce;

    public void setMarket(String market) {
        Market = market;
    }

    public void setClientId(String clientid) {
        ClientId = clientid;
    }

    public void setTimingForce(TimingForce timingForce) {TimingForce = timingForce;}

    public void setType(String type) {
        Type = type;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public void setQuantityIso(String quantityIso) {
        QuantityIso = quantityIso;
    }

    public void setRate(double rate) {
        Rate = rate;
    }

    public String getMarket() {
        return Market;
    }

    public String getClientId() {
        return ClientId;
    }
    @JsonValue
    public TimingForce getTimingForce() { return TimingForce;}

    public String getType() {
        return Type;
    }

    public int getQuantity() {
        return Quantity;
    }

    public String getQuantityIso() {
        return QuantityIso;
    }

    public double getRate() {
        return Rate;
    }
}
